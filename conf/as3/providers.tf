terraform {
  required_providers {
    bigip = {
      source = "f5networks/bigip"
      #  version = "1.8.0"
    }
    template = {
      source = "hashicorp/template"
    }
  }
  required_version = ">= 0.13"
}
