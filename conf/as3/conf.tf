/*
Copyright 2019 F5 Networks Inc.
This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

provider "bigip" {
  address  = var.addr
  username = var.user
  password = var.pass
}

resource "bigip_as3" "prereq" {
  as3_json = file("prereq.as3.json")
}

resource "bigip_as3" "common" {
  depends_on = [
    bigip_as3.prereq
  ]

  as3_json = file("common.as3.json")
}

resource "bigip_as3" "apps" {
  depends_on = [
    bigip_as3.common
  ]

  as3_json = file("apps.as3.json")
}
