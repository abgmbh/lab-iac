resource "azurerm_virtual_network" "core" {
  name                = "${var.prefix}-core"
  address_space       = [var.core-cidr]
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
}

resource "azurerm_subnet" "core-subnets" {
  count                = length(var.core-subnets)
  name                 = keys(var.core-subnets)[count.index]
  virtual_network_name = azurerm_virtual_network.core.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [values(var.core-subnets)[count.index]]
}

resource "azurerm_route_table" "udr-s4" {
  name                = "${var.prefix}-udr-s4"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  route {
    name                   = "To-alien-VIPs"
    address_prefix         = "10.1.0.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.2.4"
  }
}

resource "azurerm_subnet_route_table_association" "udr-s4" {
  subnet_id      = azurerm_subnet.core-subnets[4].id
  route_table_id = azurerm_route_table.udr-s4.id
}



