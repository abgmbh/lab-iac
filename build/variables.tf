variable "azure_client_id" {}
variable "client_certificate_path" {}
variable "client_certificate_password" {}
variable "azure_subscription_id" {}
variable "azure_tenant_id" {}
variable "location" {}

variable "prefix" { default = "cz-lab" }
variable "my-ip" {}
variable "username" {}
variable "password" {}
variable "core-cidr" { default = "10.0.0.0/16" }

variable "core-subnets" {
  type = map(any)
  default = {
    "s0" = "10.0.0.0/24"
    "s1" = "10.0.1.0/24"
    "s2" = "10.0.2.0/24"
    "s3" = "10.0.3.0/24"
    "s4" = "10.0.4.0/24"
  }
}

variable "bigip-nics" {
  type = map(any)
  default = {
    "bigip-nic0" = "10.0.0.4"
    "bigip-nic1" = "10.0.1.4"
    "bigip-nic2" = "10.0.2.4"
    "bigip-nic3" = "10.0.3.4"
  }
}

variable "ips" {
  type = map(any)
  default = {
    "bigip-sslvpn" = "10.0.1.5"
    "ubuntu"       = "10.0.4.4"
    "ubuntu1"      = "10.0.2.5"
    "win2016"      = "10.0.4.5"
  }
}

variable "pips" {
  type = list(string)
  default = [
    "bigip-mgmt",
    "bigip-external",
    "bigip-sslvpn",
    "ubuntu",
    "win2016"
  ]
}


variable "sku" { default = "f5-big-all-2slot-byol" }
variable "offer" { default = "f5-big-ip-byol" }
variable "bigip-version" { default = "latest" }
variable "instance-type" { default = "Standard_DS4_v2" }

variable "DO-URL" { default = "https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v1.25.0/f5-declarative-onboarding-1.25.0-7.noarch.rpm" }
variable "AS3-URL" { default = "https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.21.0/f5-appsvcs-3.21.0-4.noarch.rpm" }
variable "TS-URL" { default = "https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.13.0/f5-telemetry-1.13.0-2.noarch.rpm" }
variable "CF-URL" { default = "https://github.com/F5Networks/f5-cloud-failover-extension/releases/download/v1.4.0/f5-cloud-failover-1.4.0-0.noarch.rpm" }
variable "libs-dir" { default = "/config/cloud/azure/node-modules" }
variable "onboard-log" { default = "/var/log/startup-script.log" }
variable "mgmt-gw" { default = "10.0.0.1" }

variable "default_vm_tags" {
  type = map(any)
  default = {
    Owner = "christopher.zhang@f5.com"
  }
}




