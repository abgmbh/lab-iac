resource "azurerm_network_interface" "bigip-mgmt-nic" {
  name                = "${var.prefix}-bigip-mgmt-nic"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "mgmt"
    subnet_id                     = azurerm_subnet.core-subnets[0].id
    private_ip_address_allocation = "Static"
    private_ip_address            = values(var.bigip-nics)[0]
    public_ip_address_id          = azurerm_public_ip.pips[0].id
  }
}

resource "azurerm_network_interface" "bigip-ext-nic" {
  name                = "${var.prefix}-bigip-ext-nic"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "self-ip"
    subnet_id                     = azurerm_subnet.core-subnets[1].id
    private_ip_address_allocation = "Static"
    private_ip_address            = values(var.bigip-nics)[1]
    public_ip_address_id          = azurerm_public_ip.pips[1].id
    primary                       = true
  }

  ip_configuration {
    name                          = "sslvpn"
    subnet_id                     = azurerm_subnet.core-subnets[1].id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ips["bigip-sslvpn"]
    public_ip_address_id          = azurerm_public_ip.pips[2].id
  }
}

resource "azurerm_network_interface" "bigip-int-nic" {
  name                 = "${var.prefix}-bigip-int-nic"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "self-ip"
    subnet_id                     = azurerm_subnet.core-subnets[2].id
    private_ip_address_allocation = "Static"
    private_ip_address            = values(var.bigip-nics)[2]
  }
}

resource "azurerm_network_interface" "bigip-int2-nic" {
  name                 = "${var.prefix}-bigip-int2-nic"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "self-ip"
    subnet_id                     = azurerm_subnet.core-subnets[3].id
    private_ip_address_allocation = "Static"
    private_ip_address            = values(var.bigip-nics)[3]
  }
}

resource "azurerm_network_interface_security_group_association" "bigip-mgmt-ass" {
  network_interface_id      = azurerm_network_interface.bigip-mgmt-nic.id
  network_security_group_id = azurerm_network_security_group.default-nsg.id
}

resource "azurerm_network_interface_security_group_association" "bigip-ext-ass" {
  network_interface_id      = azurerm_network_interface.bigip-ext-nic.id
  network_security_group_id = azurerm_network_security_group.default-nsg.id
}
