data "terraform_remote_state" "lab" {
  backend = "remote"

  config = {
    organization = "mewomewocode"
    workspaces = {
      name = "lab-iac"
    }
  }
}

terraform {
  cloud {
    organization = "mewomewocode"

    workspaces {
      name = "lab-iac-aks"
    }
  }
}
