provider "azurerm" {
  features {}
  client_id                   = var.azure_client_id
  client_certificate_path     = var.client_certificate_path
  client_certificate_password = var.client_certificate_password
  subscription_id             = var.azure_subscription_id
  tenant_id                   = var.azure_tenant_id
}

resource "azurerm_kubernetes_cluster" "default" {
  name                = "${var.prefix}-aks"
  location            = data.terraform_remote_state.lab.outputs.rg.location
  resource_group_name = data.terraform_remote_state.lab.outputs.rg.name
  dns_prefix          = "meowmeowcode"
  kubernetes_version  = "1.22.6"

  default_node_pool {
    name            = "default"
    node_count      = 1
    vm_size         = "Standard_D2_v2"
    os_disk_size_gb = 30
  }

  identity {
    type = "SystemAssigned"
  }

  role_based_access_control_enabled = true

  tags = {
    for k, v in merge({
      Name = "${var.prefix}-aks"
      },
    var.default_vm_tags) : k => v
  }
}

