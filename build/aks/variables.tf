variable "azure_client_id" {}
variable "client_certificate_path" {}
variable "client_certificate_password" {}
variable "azure_subscription_id" {}
variable "azure_tenant_id" {}
variable "location" {}

variable "default_vm_tags" {
  type = map(any)
  default = {
    Owner = "christopher.zhang@f5.com"
  }
}

variable "prefix" { default = "cz-lab" }



