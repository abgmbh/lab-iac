resource "azurerm_public_ip" "pips" {
  count               = length(var.pips)
  name                = "${var.prefix}-${var.pips[count.index]}"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"
}
