output "subnets" {
  value = azurerm_subnet.core-subnets
}

output "rg" {
  value = azurerm_resource_group.main
}

