resource "azurerm_marketplace_agreement" "f5" {
  publisher = "f5-networks"
  offer     = var.offer
  plan      = var.sku
}

data "template_file" "vm-onboard" {
  template = file("${path.module}/onboard.tpl")

  vars = {
    admin_user     = var.username
    admin_password = var.password
    DO_URL         = var.DO-URL
    AS3_URL        = var.AS3-URL
    TS_URL         = var.TS-URL
    CF_URL         = var.CF-URL
    libs_dir       = var.libs-dir
    onboard_log    = var.onboard-log
    mgmt_gw        = var.mgmt-gw
  }
}

resource "azurerm_linux_virtual_machine" "bigip-vm" {
  name                = "${var.prefix}-bigip-vm"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  network_interface_ids = [
  azurerm_network_interface.bigip-mgmt-nic.id, azurerm_network_interface.bigip-ext-nic.id, azurerm_network_interface.bigip-int-nic.id, azurerm_network_interface.bigip-int2-nic.id]
  size                            = var.instance-type
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-bigip-vm"
  custom_data                     = base64encode(data.template_file.vm-onboard.rendered)

  os_disk {
    name                 = "${var.prefix}-bigip-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "f5-networks"
    offer     = var.offer
    sku       = var.sku
    version   = var.bigip-version
  }

  plan {
    name      = var.sku
    publisher = "f5-networks"
    product   = var.offer
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    for k, v in merge({
      Name = "${var.prefix}-bigip-vm"
      },
    var.default_vm_tags) : k => v
  }

}

resource "azurerm_role_assignment" "bigip-vm-role-ass" {
  scope                = azurerm_resource_group.main.id
  role_definition_name = "Contributor"
  principal_id         = lookup(azurerm_linux_virtual_machine.bigip-vm.identity[0], "principal_id")
}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "pm7" {
  virtual_machine_id = azurerm_linux_virtual_machine.bigip-vm.id
  location           = azurerm_resource_group.main.location
  enabled            = true

  daily_recurrence_time = "1900"
  timezone              = "AUS Eastern Standard Time"

  notification_settings {
    enabled = false
  }
}


