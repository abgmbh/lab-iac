resource "azurerm_network_interface" "ubuntu-nic" {
  name                 = "${var.prefix}-ubuntu-nic"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true


  ip_configuration {
    name                          = "ext"
    subnet_id                     = azurerm_subnet.core-subnets[4].id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ips["ubuntu"]
    public_ip_address_id          = azurerm_public_ip.pips[3].id
  }
}

resource "azurerm_network_interface" "ubuntu1-nic" {
  name                 = "${var.prefix}-ubuntu1-nic"
  location             = azurerm_resource_group.main.location
  resource_group_name  = azurerm_resource_group.main.name
  enable_ip_forwarding = true


  ip_configuration {
    name                          = "s2"
    subnet_id                     = azurerm_subnet.core-subnets[2].id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ips["ubuntu1"]
  }
}

resource "azurerm_linux_virtual_machine" "ubuntu-vm" {
  name                = "${var.prefix}-ubuntu-vm"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  network_interface_ids = [
    azurerm_network_interface.ubuntu-nic.id,
    azurerm_network_interface.ubuntu1-nic.id,
  ]
  size                            = var.instance-type
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-ubuntu-vm"

  os_disk {
    name                 = "${var.prefix}-ubuntu-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }

  tags = {
    for k, v in merge({
      Name = "${var.prefix}-ubuntu-vm"
      },
    var.default_vm_tags) : k => v
  }

}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "ubuntu-pm7" {
  virtual_machine_id = azurerm_linux_virtual_machine.ubuntu-vm.id
  location           = azurerm_resource_group.main.location
  enabled            = true

  daily_recurrence_time = "1900"
  timezone              = "AUS Eastern Standard Time"

  notification_settings {
    enabled = false
  }
}

resource "azurerm_network_interface_security_group_association" "ubuntu-ass" {
  network_interface_id      = azurerm_network_interface.ubuntu-nic.id
  network_security_group_id = azurerm_network_security_group.default-nsg.id
}
