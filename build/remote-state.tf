terraform {
  cloud {
    organization = "mewomewocode"

    workspaces {
      name = "lab-iac"
    }
  }
}
