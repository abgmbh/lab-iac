data "terraform_remote_state" "aks" {
  backend = "remote"

  config = {
    organization = "mewomewocode"
    workspaces = {
      name = "lab-iac-aks"
    }
  }
}

terraform {
  cloud {
    organization = "mewomewocode"

    workspaces {
      name = "lab-iac-argocd"
    }
  }
}
