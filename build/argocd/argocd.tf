terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "kubernetes" {
  host                   = data.terraform_remote_state.aks.outputs.aks.kube_config[0].host
  client_key             = base64decode(data.terraform_remote_state.aks.outputs.aks.kube_config[0].client_key)
  client_certificate     = base64decode(data.terraform_remote_state.aks.outputs.aks.kube_config[0].client_certificate)
  cluster_ca_certificate = base64decode(data.terraform_remote_state.aks.outputs.aks.kube_config[0].cluster_ca_certificate)
}

provider "kubectl" {
  load_config_file       = false
  host                   = data.terraform_remote_state.aks.outputs.aks.kube_config[0].host
  client_key             = base64decode(data.terraform_remote_state.aks.outputs.aks.kube_config[0].client_key)
  client_certificate     = base64decode(data.terraform_remote_state.aks.outputs.aks.kube_config[0].client_certificate)
  cluster_ca_certificate = base64decode(data.terraform_remote_state.aks.outputs.aks.kube_config[0].cluster_ca_certificate)
}

resource "kubernetes_namespace" "argocd" {
  metadata {
    name = "argocd"
  }
}

data "http" "argocd_install_manifest_url" {
  url = var.argocd_install_manifest_url
}

data "kubectl_file_documents" "argocd" {
  content = data.http.argocd_install_manifest_url.body
}

# Install Argo CD
resource "kubectl_manifest" "argocd" {
  depends_on         = [kubernetes_namespace.argocd]
  wait               = true
  count              = length(data.kubectl_file_documents.argocd.documents)
  yaml_body          = element(data.kubectl_file_documents.argocd.documents, count.index)
  override_namespace = "argocd"
}

resource "kubectl_manifest" "test" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  name: argocd-lb
  namespace: argocd
  labels:
    app.kubernetes.io/component: server
    app.kubernetes.io/name: argocd-server
    app.kubernetes.io/part-of: argocd
spec:
  externalTrafficPolicy: Local
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 8080
    protocol: TCP
    name: http
  - port: 443
    targetPort: 8080
    protocol: TCP
    name: https
  selector:
    app.kubernetes.io/name: argocd-server
YAML
}



