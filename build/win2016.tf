resource "azurerm_network_interface" "win2016-nic" {
  name                = "${var.prefix}-win2016-nic"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "ext"
    subnet_id                     = azurerm_subnet.core-subnets[4].id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ips["win2016"]
    public_ip_address_id          = azurerm_public_ip.pips[4].id
  }
}

resource "azurerm_windows_virtual_machine" "win2016-vm" {
  name                = "${var.prefix}-win2016-vm"
  computer_name       = "${var.prefix}-win2016"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = "Standard_F2"
  admin_username      = var.username
  admin_password      = var.password
  network_interface_ids = [
    azurerm_network_interface.win2016-nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  tags = {
    for k, v in merge({
      Name = "${var.prefix}-win2016-vm"
      },
    var.default_vm_tags) : k => v
  }

}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "win2016-pm7" {
  virtual_machine_id = azurerm_windows_virtual_machine.win2016-vm.id
  location           = azurerm_resource_group.main.location
  enabled            = true

  daily_recurrence_time = "1900"
  timezone              = "AUS Eastern Standard Time"

  notification_settings {
    enabled = false
  }
}

resource "azurerm_network_interface_security_group_association" "win2016-ass" {
  network_interface_id      = azurerm_network_interface.win2016-nic.id
  network_security_group_id = azurerm_network_security_group.default-nsg.id
}
